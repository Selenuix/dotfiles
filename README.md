# Dotfile

Mon dotfile Ubuntu.

TODO :
- Tests en VM
- Ajouter et supprimer des logiciels
- Mettre à jour la branch Archlinux

## A propos

Cela fait maintenant deux-trois ans que j'utilise GNU/Linux au quotidien et je commence à recontrer un sérieux problème lorsque je doit réinstaller ma distribution ou que je change pour une autre. Soit je réinstalle tous les paquets une fois la distribution installée (ce qui est assez long à faire, parce taper le nom de tous les principaux paquets que j'utilise prend du temps), soit je garde la distribution telle-qu'elle et je ne change que peu de choses au final (cette manière est également embêtante, car l'interface et les logiciels ne sont pas "uniformisés" sur toutes mes ditributions. C'est pourquoi j'ai décidé de créer mon Dotfile. J'ai cherché un peu ce qui se faisait et je suis tombé, la plupart du temps, sur des dépôts avec des fichiers de config, même pas de scripts d'installation. Je me suis en suite dit que si je devais tout bouger à la main, ça serait un peu long et compliqué. J'ai ensuite commencé à écrire un petit script bash, afin d'installer les logiciels tout seuls, et bouger certains fichiers de config. Je me suis dit ensuite que je pouvais faire mieux et plus "propre". 

Je suis ensuite tombé sur le Dotfile de [cowboy](https://github.com/cowboy/ "Son github") que j'ai forké. Le dépôt proposait toute une partie pour MacOS que j'ai supprimé (n'ayant pas de Mac et n'utilisant pas ce système), ainsi que la partie Ubuntu que j'ai gardé. J'ai juste supprimé tous les fichiers de config et les logiciels que je ne voulais pas (Ruby, Node, Vagrant, ...) et j'ai ajouté ce que j'utilisais.

Pour améliorer un peu tout ça, et vu que je ne tourne pas exclusivement sur des distributions basées sur Ubuntu (et non Debian, car le script utilise les PPA), je prépare une version pour Archlinux. Etant actuellement sous Fedora, je pense qu'une version verra le jour si cette distribution continue de me plaîre d'içi là.

Commme le dit cowboy dans son README, la seule commande à utiliser est [dotfiles][dotfiles] ;)
A partir de là, je vais simplement me contenter de traduire le README de cowboy et y apporter quelques modifications.

[dotfiles]: bin/dotfiles

## Comment marche la commande "Dotfile"

Quand [dotfiles][dotfiles] est lancé pour la première fois, il effectue quelques tâches :

1. Git est installé,
1. Le dépôt est cloné dans votre répertoire personel, sous `~/.dotfiles`. 
1.Les fichiers dans `/copy` sont copiés dans `~/`. ([en savoir plus](### L'étape "copy"))
1. Les fichiers dans `/link` sont symlinkés dans `~/`. ([en savoir plus](### L'étape "link"))
1. Il vous demande quels scripts de `/init` vous voulez executer. Le programme d'installation tente de sélectionner uniquement les scripts pertinents, en fonction du système d'exploitation détecté et du nom de fichier du script.
1. Vos scripts d'installation sont lancé (dans l'ordre alphabétique, d'où les nom bizzare). ([en savoir plus](### L'étape "init"))

Sur les lacements suivants, l'étape 1 est ignorée, l'étape 2 vient mettre à jour le dépôt déjà existant, et l'étape 5 rappelle ce que vous avez sélectionné la dernière fois. Les autres étapes sont les mêmes.

### Autres sous-répertoires

* Le répertoire `/ backups` est créé si nécessaire. Tous les fichiers dans `~ /` qui auraient été écrasés par des fichiers dans `/ copy` ou` / link` sont sauvegardés là-bas.
* Le répertoire `/ bin` contient des scripts de shell exécutables (y compris le script [dotfiles] [dotfiles]) et des liens symboliques vers des scripts shell exécutables. Ce répertoire est ajouté au chemin d'accès.
* Le répertoire `/ caches` contient des fichiers mis en cache, utilisés par certains scripts ou fonctions.
* Le répertoire `/ conf` existe juste. Si un fichier de configuration n'a pas ** besoin ** d'entrer dans `~ /`, faites-le référence à partir du répertoire `/ conf`.
* Le répertoire `/ source` contient des fichiers qui proviennent chaque fois qu'un nouveau shell est ouvert (dans l'ordre alphanumérique, d'où les noms bizzare).
* Le répertoire `/ test` contient des tests unitaires pour des fonctions bash particulièrement compliquées.
* Le répertoire `/ vendor` contient des bibliothèques tierces.

### L'étape "copy"
Tout fichier dans le sous-répertoire `/ copy` sera copié dans` ~/`. Tout fichier qui _doit_ être modifié avec des informations personnelles (comme [copy / .gitconfig] (copy /.gitconfig) qui contient une adresse e-mail et une clé privée) doit être _copied_ dans `~/`. Étant donné que le fichier que vous éditez n'est plus dans `~/.dotfiles`, il est moins susceptible d'être impliqué accidentellement dans votre dépôt dotfiles public.

### L'étape "link"
Tout fichier dans le sous-répertoire `/link` est symlinké dans`~/`avec` ln -s`. Modifiez l'un ou l'autre, et vous changez le fichier dans les deux endroits. Ne liez pas les fichiers contenant des données sensibles, ou vous pouvez accidentellement commit ces données ! Si vous reliez un répertoire qui peut contenir des données sensibles (comme `~/.ssh`), ajoutez les fichiers sensibles à votre fichier [.gitignore] (.gitignore)!

### L'étape "init"
Les scripts dans le sous-répertoire `/init` seront exécutés. Tout un tas de choses seront installées, mais _seulement_ si elles ne le sont pas déjà.

#### Ubuntu
* Paquets APT et git-extras via le script [init/20_ubuntu_apt.sh] (init/20_ubuntu_apt.sh)
* Plugins Vim via le script [init/50_vim.sh] (init/50_vim.sh)

## Hacker mon dotfiles

Parce que le script [dotfiles] [dotfiles] est complètement autonome, vous devriez pouvoir supprimer tout le reste de votre fork, cela fonctionnera toujours. La seule chose dont il se soucie vraiment sont les sous-répertoires `/copy`,` /link` et `/init`, qui seront ignorés s'ils sont vides ou n'existent pas.

Si vous modifiez des choses et notez un bug ou une amélioration, [ouvrez une issue] (https://github.com/cowboy/dotfiles/issues) ou [faite une pull request] (https://github.com/Selenuix/dotfiles/pulls) et dites le moi.

Aussi, avant d'installer, assurez-vous de [lire ma note] (# heed-this-critical-important-warning-before-you-install).

## Installation
### Ubuntu

Vous pouvez configurer votre serveur ubuntu [comme je le fais] (https://github.com/cowboy/dotfiles/wiki/ubuntu-setup), mais encore une fois, vous pourriez ne pas le faire.

Quoi qu'il en soit, vous devez au moins mettre à jour / mettre à niveau APT avec `sudo apt-get -qq update && sudo apt-get -qq dist-upgrade` en premier.

### A lire avant installation

** Si vous n'êtes pas moi, n'installer pas directement le Dotfile à partir de ce dépôt ! **
Pourquoi? Parce que je pète souvent le dépôt lors de la mise à jour. Ce qui signifie que si je le fais et que vous exécutez la commande `dotfiles`, votre répertoire personnel va exploser et vous devrez aller acheter un nouvel ordinateur. Non, pas vraiment, mais ce sera très désordonné.

### Installation (pour vous)

1. [Lisez la note](### A lire avant installation)
1. Forkez le dépôt
1. Ouvrez un terminal et faite (changez `Selenuix` et `master` avec vos paramètres) :

#### Ubuntu

```sh
export DOTFILES_GH_USER=Selenuix
export DOTFILES_GH_BRANCH=master
bash -c "$(wget -qO- https://raw.github.com/$DOTFILES_GH_USER/dotfiles/$DOTFILES_GH_BRANCH/bin/dotfiles)" && source ~/.bashrc
```

Étant donné que vous utiliserez la commande [dotfiles] [dotfiles] sur les exécutions suivantes, il vous suffit de définir la variable `DOTFILES_GH_USER` pour l'installation initiale, mais si vous avez une branche personnalisée, vous devrez exporter` DOTFILES_GH_BRANCH` Pour les lancements suivants.

Il y a beaucoup de choses qui nécessitent un accès administrateur via `sudo`, alors soyez averti que vous devrez peut-être entrer votre mot de passe ici ou là.

### Installation (pour moi)

#### Ubuntu

```sh
bash -c "$(wget -qO- https://bit.ly/cowboy-dotfiles)" && source ~/.bashrc
```

## Alias et fonctions
Pour simplifier les choses, les fichiers `~/.bashrc` et` ~/.bash_profile` sont extrêmement simples et ne devront jamais être modifiés. Au lieu de cela, ajoutez vos alias, fonctions, paramètres, etc. dans l'un des fichiers dans le sous-répertoire `source` ou ajoutez un nouveau fichier. Ils sont tous automatiquement mis à jour quand un nouveau shell est ouvert. Regardez, j'ai [beaucoup d'alias et de fonctions] (source). J'ai même une [Terminal fantesiste] (source/50_prompt.sh) qui affiche le répertoire actuel, l'heure et l'état actuel de git / svn.

## Scripts
En plus du script [dotfiles] [dotfiles], il existe quelques autres [scripts bin](bin). Cela comprend [nave] (https://github.com/isaacs/nave), qui est un [sous-module git] (vendor).

* [dotfiles][dotfiles] -  (re) initialiser dotfiles. Il pourrait demander votre mot de passe (pour `sudo`).
* [src](link/.bashrc#L8-18) - (re) source tous les fichiers dans le répertoire `/source`.
* Regardez le sous-répertoire [bin](bin) pour plus d'infos.

## Prompt
Je pense que [mon prompt bash](source/50_prompt.sh) est génial. Il affiche l'état de la prise en charge git et svn, un timestamp, des codes de sortie d'erreur et même des changements de couleur selon la connexion.

Le dépôt Git affiche **[branch:flags]** où les drapeaux sont : 

**?** fichier non ajouté
**!** fichier modifié (mais non ajouté)  
**+** fichier ajoutés

Le dépôt SVN affiche **[rev1:rev2]** où rev1 et rev2 sont :

**rev1** denière révision changée  
**rev2** révision

Vérifié le :
![Mon magnifique prompt bash](http://farm8.staticflickr.com/7142/6754488927_563dd73553_b.jpg)

## Inspiration
<https://github.com/gf3/dotfiles>  
<https://github.com/mathiasbynens/dotfiles>  
(Et plus de 15 ans de crasse accumulée)

## License
Licensed under the WTFPL license.  
<http://www.wtfpl.net/txt/copying/>
